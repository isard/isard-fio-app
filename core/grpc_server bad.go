package core

/*
import (
	"io"
	"net"
	"os"
	"strconv"

	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/spf13/afero"
	"gitlab.com/isard-vdi/fio/uploadresults/messaging"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	_ "google.golang.org/grpc/encoding/gzip"
)

type ServerGRPC struct {
	logger      zerolog.Logger
	server      *grpc.Server
	port        int
	certificate string
	key         string
}

type ServerGRPCConfig struct {
	Certificate string
	Key         string
	Port        int
}

func NewServerGRPC(cfg ServerGRPCConfig) (s ServerGRPC, err error) {
	s.logger = zerolog.New(os.Stdout).
		With().
		Str("from", "server").
		Logger()

	if cfg.Port == 0 {
		err = errors.Errorf("Port must be specified")
		return
	}

	s.port = cfg.Port
	s.certificate = cfg.Certificate
	s.key = cfg.Key

	return
}

func (s *ServerGRPC) Listen() (err error) {
	var (
		listener  net.Listener
		grpcOpts  = []grpc.ServerOption{}
		grpcCreds credentials.TransportCredentials
	)

	listener, err = net.Listen("tcp", ":"+strconv.Itoa(s.port))
	if err != nil {
		err = errors.Wrapf(err,
			"failed to listen on port %d",
			s.port)
		return
	}

	if s.certificate != "" && s.key != "" {
		grpcCreds, err = credentials.NewServerTLSFromFile(
			s.certificate, s.key)
		if err != nil {
			err = errors.Wrapf(err,
				"failed to create tls grpc server using cert %s and key %s",
				s.certificate, s.key)
			return
		}

		grpcOpts = append(grpcOpts, grpc.Creds(grpcCreds))
	}

	s.server = grpc.NewServer(grpcOpts...)
	messaging.RegisterGuploadServiceServer(s.server, s)

	err = s.server.Serve(listener)
	if err != nil {
		err = errors.Wrapf(err, "errored listening for grpc connections")
		return
	}

	return
}

func (s *ServerGRPC) Upload(stream messaging.GuploadService_UploadServer) (err error) {
	var res *messaging.Chunk
	var fo *os.File
	var dir string
	var filename string
	for {
		res, err = stream.Recv()

		if err == io.EOF {
			err = stream.SendAndClose(&messaging.UploadStatus{
				Message: "Upload received with success: " + filename,
				Code:    messaging.UploadStatusCode_Ok,
			})
			if err != nil {
				err = errors.New("failed to send status code")
				return err
			}
			return nil
		}

		dir = "./uploads/" + res.Folderid
		filename = dir + "/" + res.Filename

		if err = createPath(dir); err != nil {
			err = stream.SendAndClose(&messaging.UploadStatus{
				Message: "Folder " + dir + " can't be created!",
				Code:    messaging.UploadStatusCode_Failed,
			})
			return err
		}
		if fileExists(filename) {
			err = stream.SendAndClose(&messaging.UploadStatus{
				Message: "File " + filename + " already exists! Skipping.",
				Code:    messaging.UploadStatusCode_Failed,
			})
			return err
		} else {
			fo, err := os.Create(filename)
			if err != nil {
				err = stream.SendAndClose(&messaging.UploadStatus{
					Message: "File " + filename + " can't be created!.",
					Code:    messaging.UploadStatusCode_Failed,
				})
				return err
			}
			defer func() {
				if err := fo.Close(); err != nil {
					s.logger.Info().Msg("Error closing")
					panic(err)
				}
			}()
		}


		// write a chunk
		s.logger.Info().Msg("Write chunk")
		if _, err := fo.Write(res.Content); err != nil {
			err = errors.New(err.Error())
			s.logger.Info().Msg(err.Error())
			return err
		}
	}
}

func (s *ServerGRPC) Close() {
	if s.server != nil {
		s.server.Stop()
	}

	return
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func createPath(path string) error {
	if _, err := os.Stat(path); errors.Is(err, afero.ErrFileNotFound) {
		if err = os.MkdirAll(path, 0755); err != nil {
			err = errors.Wrapf(err,
				"failed to create folder %s",
				path)
			return err
		}
	}
	return nil
}
*/
