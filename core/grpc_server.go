package core

import (
	"fmt"
	"io"
	"net"
	"os"
	"strconv"

	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/spf13/afero"
	"gitlab.com/isard-vdi/fio/uploadresults/messaging"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	_ "google.golang.org/grpc/encoding/gzip"
)

type ServerGRPC struct {
	logger      zerolog.Logger
	server      *grpc.Server
	port        int
	certificate string
	key         string
}

type ServerGRPCConfig struct {
	Certificate string
	Key         string
	Port        int
}

func NewServerGRPC(cfg ServerGRPCConfig) (s ServerGRPC, err error) {
	s.logger = zerolog.New(os.Stdout).
		With().
		Str("from", "server").
		Logger()

	if cfg.Port == 0 {
		err = errors.Errorf("Port must be specified")
		return
	}

	s.port = cfg.Port
	s.certificate = cfg.Certificate
	s.key = cfg.Key

	return
}

func (s *ServerGRPC) Listen() (err error) {
	var (
		listener  net.Listener
		grpcOpts  = []grpc.ServerOption{}
		grpcCreds credentials.TransportCredentials
	)

	listener, err = net.Listen("tcp", ":"+strconv.Itoa(s.port))
	if err != nil {
		err = errors.Wrapf(err,
			"failed to listen on port %d",
			s.port)
		return
	}

	if s.certificate != "" && s.key != "" {
		grpcCreds, err = credentials.NewServerTLSFromFile(
			s.certificate, s.key)
		if err != nil {
			err = errors.Wrapf(err,
				"failed to create tls grpc server using cert %s and key %s",
				s.certificate, s.key)
			return
		}

		grpcOpts = append(grpcOpts, grpc.Creds(grpcCreds))
	}

	s.server = grpc.NewServer(grpcOpts...)
	messaging.RegisterGuploadServiceServer(s.server, s)

	err = s.server.Serve(listener)
	if err != nil {
		err = errors.Wrapf(err, "errored listening for grpc connections")
		return
	}

	return
}

func (s *ServerGRPC) Upload(stream messaging.GuploadService_UploadServer) (err error) {
	// open output file
	/* 	fo, err := os.Create("./output")
	   	if err != nil {
	   		return errors.New("failed to create file")
	   	} */
	// close fo on exit and check for its returned error
	/* 	defer func() {
		if err := fo.Close(); err != nil {
			panic(err)
		}
	}() */
	var res *messaging.Chunk
	var fo *os.File
	chunknumber := 0
	for {
		res, err = stream.Recv()

		if err == io.EOF {
			err = stream.SendAndClose(&messaging.UploadStatus{
				Message: "Upload received with success",
				Code:    messaging.UploadStatusCode_Ok,
			})
			if err != nil {
				err = errors.New("failed to send status code")
				return err
			}
			return nil
		}

		dir := "./uploads/" + res.Company + "/" + res.Test

		if chunknumber == 0 {
			fmt.Println("Received new file: " + dir + "/" + res.Filename)
			if _, err = os.Stat(dir); errors.Is(err, afero.ErrFileNotFound) {
				if err = os.MkdirAll(dir, 0755); err != nil {
					err = errors.Wrapf(err,
						"failed to create folder %s",
						dir)
					s.logger.Info().Msg(err.Error())
					return
				}
			}

			fo, err = os.Create(dir + "/" + res.Filename)
			if err != nil {
				s.logger.Info().Msg(err.Error())
				return errors.New("failed to create file")
			}
			defer func() {
				if err := fo.Close(); err != nil {
					panic(err)
				}
			}()
		}
		chunknumber = chunknumber + 1
		// write a chunk
		if _, err := fo.Write(res.Content); err != nil {
			err = errors.New(err.Error())
			s.logger.Info().Msg(err.Error())
			return err
		}
	}

	/* var rcv *messaging.Chunk
		for {
			rcv, err = stream.Recv()
			if err != nil {
				if err == io.EOF {
					goto END
				}

				err = errors.Wrapf(err,
					"failed unexpectadely while reading chunks from stream")
				return
			}

			dir := "./uploads/" + rcv.Folderid
			s.logger.Info().Msg(dir)
			if _, err = os.Stat(dir); errors.Is(err, afero.ErrFileNotFound) {
				if err = os.MkdirAll(dir, 0755); err != nil {
					err = errors.Wrapf(err,
						"failed to create folder %s",
						rcv.Folderid)
					s.logger.Info().Msg(err.Error())
					return
				}
			}
			s.logger.Info().Msg(rcv.Folderid + " - " + rcv.Filename)
		}
		s.logger.Info().Msg(rcv.Filename)
	END:
		err = stream.SendAndClose(&messaging.UploadStatus{
			Message: "Upload received with success",
			Code:    messaging.UploadStatusCode_Ok,
		})
		if err != nil {
			err = errors.Wrapf(err,
				"failed to send status code")
			return
		}

		s.logger.Info().Msg("upload received: " + "./uploads/" + rcv.Folderid + "/" + rcv.Filename)
		return */
}

func (s *ServerGRPC) Close() {
	if s.server != nil {
		s.server.Stop()
	}

	return
}
