module gitlab.com/isard-vdi/fio/uploadresults

go 1.14

require (
	github.com/gogo/protobuf v1.3.1
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.19.0
	github.com/spf13/afero v1.2.2
	github.com/urfave/cli/v2 v2.2.0
	golang.org/x/net v0.0.0-20200528225125-3c3fba18258b
	google.golang.org/grpc v1.29.1
)
