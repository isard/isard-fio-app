package cmd

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"time"

	"github.com/urfave/cli/v2"
	"gitlab.com/isard-vdi/fio/uploadresults/core"
	"golang.org/x/net/context"
)

var Upload = cli.Command{
	Name:   "upload",
	Usage:  "uploads a file",
	Action: uploadAction,
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:  "address",
			Value: "localhost:1313",
			Usage: "address of the server to connect to",
		},
		&cli.IntFlag{
			Name:  "chunk-size",
			Usage: "size of the chunk messages (grpc only)",
			Value: (1 << 12),
		},
		&cli.StringFlag{
			Name:  "test",
			Usage: "name of test",
		},
		&cli.StringFlag{
			Name:  "id",
			Usage: "name of id",
		},
		&cli.StringFlag{
			Name:  "path",
			Usage: "path of files to upload",
		},
		&cli.StringFlag{
			Name:  "company",
			Usage: "identifier for this files on server (will add date automatically)",
		},
		&cli.StringFlag{
			Name:  "root-certificate",
			Usage: "path of a certificate to add to the root CAs",
		},
		&cli.BoolFlag{
			Name:  "http2",
			Usage: "whether or not to use http2 - requires root-certificate",
		},
		&cli.BoolFlag{
			Name:  "compress",
			Usage: "whether or not to enable payload compression",
		},
	},
}

func uploadAction(c *cli.Context) (err error) {
	var (
		chunkSize       = c.Int("chunk-size")
		http2           = c.Bool("http2")
		address         = c.String("address")
		test            = c.String("test")
		id              = c.String("id")
		path            = c.String("path")
		company         = c.String("company")
		rootCertificate = c.String("root-certificate")
		compress        = c.Bool("compress")
		client          core.Client
	)

	if address == "" {
		must(errors.New("address"))
	}

	if company == "" {
		must(errors.New("company name must be set"))
	}

	if path == "" {
		must(errors.New("path of files to upload must be set"))
	}

	if test == "" {
		must(errors.New("test name for files to upload must be set"))
	}

	if id == "" {
		must(errors.New("id for files to upload must be set"))
	}

	switch {
	case http2:
		if rootCertificate == "" {
			must(errors.New("http2 requires root-certificate to be supplied"))
		}

		if !strings.HasPrefix(address, "https://") {
			address = "https://" + address
		}

		http2Client, err := core.NewClientH2(core.ClientH2Config{
			Address:         address,
			RootCertificate: rootCertificate,
		})
		must(err)
		client = &http2Client
	default:
		grpcClient, err := core.NewClientGRPC(core.ClientGRPCConfig{
			Address:         address,
			RootCertificate: rootCertificate,
			Compress:        compress,
			ChunkSize:       chunkSize,
		})
		must(err)
		client = &grpcClient
	}

	fmt.Printf("Looking for files in %s path...\n", path)

	files, err := ioutil.ReadDir("./" + path)
	if err != nil {
		log.Fatal(err)
	}

	t := time.Now()
	timenow := fmt.Sprintf("%d-%02d-%02dT%02d_%02d_%02d",
		t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())

	for _, file := range files {
		fmt.Println(file.Name() + "\n")
		stat, err := client.UploadFile(context.Background(), company, id+"/"+timenow+"_"+test, path, file.Name())
		defer client.Close()
		must(err)
		fmt.Printf("%d\n", stat.FinishedAt.Sub(stat.StartedAt).Nanoseconds())
	}
	return
}
